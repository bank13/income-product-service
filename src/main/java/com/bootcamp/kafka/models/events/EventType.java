package com.bootcamp.kafka.models.events;

public enum EventType {
  CREATED, UPDATED, DELETED
}