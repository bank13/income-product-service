package com.bootcamp.incomeproductservice.controller;


import com.bootcamp.incomeproductservice.service.ProducerEventsService;
import com.bootcamp.incomeproductservice.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/incomes/broker")
public class MessageBrokerController {

  @Autowired
  private ProducerService messageBroker;

  @PostMapping(value = "/publish")
  public String sendMessageToKafkaTopic(@RequestParam("message") String message) {
    return this.messageBroker.save(message);
  }

}
