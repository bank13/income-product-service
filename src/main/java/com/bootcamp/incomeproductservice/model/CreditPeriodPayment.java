package com.bootcamp.incomeproductservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Document(collection = "CreditPeriodPayment")

public class CreditPeriodPayment {
  @Id
  private String creditPeriodDebtID;
  private String creditID;
  private double finalAmount;
  private double penalties;
  private LocalDateTime debtStartDate;
  private LocalDateTime debtEndDate;
  private String status;
  private boolean isDue;
}
