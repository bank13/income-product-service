package com.bootcamp.incomeproductservice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data

public class CreditPeriodPaymentDto {
  private String creditPeriodDebtID;
  private String creditID;
  private double finalAmount;
  private double penalties;
  private LocalDateTime debtStartDate;
  private LocalDateTime debtEndDate;
  private String status;
  private boolean isDue;
}
