package com.bootcamp.incomeproductservice.service.impl;

import com.bootcamp.incomeproductservice.service.ProducerEventsService;
import com.bootcamp.incomeproductservice.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProducerServiceImpl implements ProducerService {

  @Autowired
  private ProducerEventsService producerEventsService;


  /** save method.
   * @param message String
   * @return String
   */
  @Override
  public String save(String message) {
    System.out.println("Received " + message);
    this.producerEventsService.publish(message);
    return message;
  }
}
