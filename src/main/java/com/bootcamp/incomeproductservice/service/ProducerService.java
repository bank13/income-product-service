package com.bootcamp.incomeproductservice.service;

import org.springframework.stereotype.Service;

@Service
public interface ProducerService {

  String save(String message);
}
