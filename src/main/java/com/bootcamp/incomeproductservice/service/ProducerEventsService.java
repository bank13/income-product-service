package com.bootcamp.incomeproductservice.service;

import com.bootcamp.kafka.models.events.Event;
import com.bootcamp.kafka.models.events.EventType;
import com.bootcamp.kafka.models.events.StringEvent;
import java.util.Date;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class ProducerEventsService {

  private static final Logger logger = LoggerFactory.getLogger(ProducerEventsService.class);

  @Autowired
  private KafkaTemplate<String, Event<?>> producer;


  //@Value("${topic.bank.name:incomes}")
  @Value("incomes")
  private String topicCustomer;

  public ProducerEventsService(KafkaTemplate<String, Event<?>> producer) {
    this.producer = producer;
  }

  /**
   * publish method.
   *
   * @param message String
   */
  public void publish(String message) {
    logger.info("Producing message {}", message);

    StringEvent event = new StringEvent();
    event.setDate(new Date());
    event.setId(UUID.randomUUID().toString());
    event.setType(EventType.CREATED);
    event.setData(message);
    producer.send(topicCustomer, event);
  }
}
